FROM ubuntu:latest
LABEL David Desjardins "david.desjardins@comcast.net"
RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev build-essential

#Add source files
COPY . /app
ENV HOME=/app
WORKDIR /app

# Install Python web server and dependencies
RUN pip3 install -r requirements.txt

ENV FLASK_APP=app.py

# Expose port
EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["app.py"]
#!/bin/bash

docker stop task-manager-app
docker rm task-manager-app
docker rmi -f task-manager-app
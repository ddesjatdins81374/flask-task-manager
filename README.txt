This is a python web app that allows you to add, delete, and update tasks
Author: David Desjardins
Run it like this: python3 app.py e.g.
python3 app.py
Once the app is running, open a web browser to http://localhost:5000

##### How to run in a Docker Container #####

This container will run a ubuntu base image

1. Build the docker image by running the build.sh shell script
- once the build is complete issue the "docker images" command to see the task-manager-app image

2. Run the docker image by executing the run.sh shell script
- when the command runs issue the "docker ps" to see that the docker container is running

3. Open a Web browser and enter http://localhost:5000
- Here you will be able to add, update, amd delete tasks

4. To clean up and remove the docker container execute the clean-docker.sh shell script
- When this command completes issue the "docker images" command to see that the image is deleted

##### How to run the App Locally #####

1. Install the python app dependecies
- run the following: pip install -r requirements.txt

2. Start the Python Task Manager App
- run the following: python3 app.py

3. Access the Python Task Manager App
- Enter the following into a Web Browser: http://localhost:5000
- Once running add a task to the text box and click add
- You can now add, update, and delete tasks

